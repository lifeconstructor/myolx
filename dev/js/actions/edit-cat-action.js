import { API_URL, ADVERT_CAT } from '../constants.js';

 export function editCatAction(dispatch, catId, postingData){
   console.log('PUT',postingData);
  return fetch(API_URL + ADVERT_CAT + catId, {
        method: 'PUT',
        mode: 'cors',
        headers: {
          //'X-HTTP-Method-Override': 'PUT',
          'Accept': 'application/json',
          'HTTP_X_REST_USERNAME' : 'admin@restuser',
          'HTTP_X_REST_PASSWORD' : 'admin@Access',
        },
        //credentials: 'same-origin',
        //body: 'AdvertCat[name]='+postingData.name+'&AdvertCat[desc]='+postingData.desc
        body: JSON.stringify(postingData)
    })
    .then(response => response.json().then(body => ({ response, body })))
    .then(({ response, body }) => {
      if (!response.ok) {
        // 404
        return body;
      } else {
        // 200
        return body;
      }
    })

}
