import { API_URL, ADVERT_CAT } from '../constants.js';

 export function filterCatsAction(dispatch){

   var filterType = arguments[1].toLowerCase();
   var filterValue = arguments[2];

   // ex: {"property": "id", "value" : 50, "operator": ">="}
   var bodySend = {"property":filterType,"value":filterValue};
   console.log(bodySend);
   // fetch
   return fetch(API_URL + ADVERT_CAT + '?filter=['+JSON.stringify(bodySend)+']', {
     method: 'GET',
     mode: 'cors',
     headers: {
       'HTTP_X_REST_USERNAME' : 'admin@restuser',
       'HTTP_X_REST_PASSWORD' : 'admin@Access',
     }
   })
   .then(response => response.json().then(body => ({ response, body })))
   .then(({ response, body }) => {

     if (!response.ok) {
       // 404
       return dispatch({
         type: 'FETCH_CATS_FAILURE',
         cats: {}
       });
     } else {
       // 200
       return dispatch({
         type: 'FETCH_CATS_SUCCESS',
         cats: body.data.advertCat
       });
     }
   })
}
