import {
  API_URL,
  ADVERT_CAT,
  CREATE_CAT_FAILURE,
  CREATE_CAT_SUCCESS
} from '../constants.js';

 export function createCatAction(dispatch, postingData){
   console.log('POST',postingData);
   //console.log('URL',API_URL + ADVERT_CAT + 'create');
  return fetch(API_URL + ADVERT_CAT, {
        method: 'POST',
        mode: 'cors',
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
          'HTTP_X_REST_USERNAME' : 'admin@restuser',
          'HTTP_X_REST_PASSWORD' : 'admin@Access',
        },
        //body: JSON.stringify(postingData)
        body: 'AdvertCat[name]='+postingData.name+'&AdvertCat[desc]='+postingData.desc
    })
    .then(response => response.json(postingData).then(body => ({ response, body })))
    .then(({ response, body }) => {
      if (!response.ok) {
        // 404
        console.log(body);
        // return body;
        return dispatch({
          type: CREATE_CAT_FAILURE
        });
      } else {
        // 200
        console.log(body);
        //return body;
        return dispatch({
            type: CREATE_CAT_SUCCESS,
            createCat: body
        });
      }
    })

}
