import { API_URL, ADVERT_CAT } from '../constants.js';

 export function deleteCatAction(catId){

  return fetch(API_URL + ADVERT_CAT + catId, {
        method: 'DELETE',
        mode: 'cors',
        headers: {
          "Content-type": "text/plain; charset=UTF-8",
          //'X-HTTP-Method-Override': 'DELETE',
          'HTTP_X_REST_USERNAME' : 'admin@restuser',
          'HTTP_X_REST_PASSWORD' : 'admin@Access',
        }
    })
    .then(response => response.json().then(body => ({ response, body })))
    .then(({ response, body }) => {
      if (!response.ok) {
        // 404
        console.log(body);
        return body;
        // return dispatch({
        //   type: 'FETCH_CATS_FAILURE',
        //   cats: {}
        // });
      } else {
        // 200
        console.log(body);
        return body;
        //return dispatch( myActionFetch(body.data.advertCat));
      }
    })

}

// fetch success and dispatched
function myActionFetch (val){
  return {
    type: 'FETCH_CATS_SUCCESS',
    cats: val
  }
}
