import { API_URL, ADVERT_CAT } from '../constants.js';

 export function advertCatsAction(dispatch){
  return fetch(API_URL + ADVERT_CAT, {
        method: 'GET',
        mode: 'cors',
        headers: {
          'HTTP_X_REST_USERNAME' : 'admin@restuser',
          'HTTP_X_REST_PASSWORD' : 'admin@Access',
        }
    })
    .then(response => response.json().then(body => ({ response, body })))
    .then(({ response, body }) => {
      if (!response.ok) {
        // 404
        return dispatch({
          type: 'FETCH_CATS_FAILURE',
          cats: {}
        });
      } else {
        // 200
        return dispatch( myActionFetch(body.data.advertCat));
      }
    })

}

// fetch success and dispatched
function myActionFetch (val){
  return {
    type: 'FETCH_CATS_SUCCESS',
    cats: val
  }
}
