export const API_URL = 'http://localhost/api/';
export const ADVERT_CAT = 'advertCat/';
export const ADVERT_ITEM = 'advertItem/';

// CRUD category actions
// r
export const FETCH_CATS_FAILURE = 'FETCH_CATS_FAILURE';
export const FETCH_CATS_SUCCESS = 'FETCH_CATS_SUCCESS';
// c
export const CREATE_CAT_SUCCESS = 'CREATE_CAT_SUCCESS';
export const CREATE_CAT_FAILURE = 'CREATE_CAT_FAILURE';
// u
export const EDIT_CAT_SUCCESS = 'EDIT_CAT_SUCCESS';
export const EDIT_CAT_FAILURE = 'EDIT_CAT_FAILURE';
// d
export const DELETE_CAT_SUCCESS = 'DELETE_CAT_SUCCESS';
export const DELETE_CAT_FAILURE = 'DELETE_CAT_FAILURE';
