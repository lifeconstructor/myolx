/*
 * All reducers get two parameters passed in, state and action that occurred
 *       > state isn't entire apps state, only the part of state that this reducer is responsible for
 * */

import Immutable from 'immutable';

const initialState = Immutable.fromJS({});

export default (state = initialState, action) => {
  switch (action.type) {
      case 'USER_SELECTED' || 'USER_EDIT':
          return action.payload;
          break;
  }
    return state;
};
