import Immutable from 'immutable';

const initialState = Immutable.fromJS({
  cats: {},
  countCats: 0,
  filterType: '',
  filterValue: ''
});

// Reducer for AdvertCats
export default (state = initialState, action) => {

 switch (action.type) {
     case 'FETCH_CATS_SUCCESS':
         return state.merge({
             cats: action.cats,
             error: 0
         });
         break;
     case 'FETCH_CATS_FAILURE':
         return state.merge({
             error: 1
         });
         break;
      default:
      return state;
 }
};
