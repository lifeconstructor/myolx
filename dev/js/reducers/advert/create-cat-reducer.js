import Immutable from 'immutable';

const initialState = Immutable.fromJS({
  createCat: {}
});

// Reducer for AdvertCats
export default (state = initialState, action) => {

 switch (action.type) {
     case 'CREATE_CAT_SUCCESS':
         return state.merge({
             error: 0,
             createCat: action.createCat
         });
         break;
     case 'CREATE_CAT_FAILURE':
         return state.merge({
             error: 1
         });
         break;
      default:
      return state;
 }
};
