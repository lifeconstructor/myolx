import Immutable from 'immutable';

const initialState = Immutable.fromJS({
  editCat: {}
});

// Reducer for AdvertCats
export default (state = initialState, action) => {

 switch (action.type) {
     case 'EDIT_CAT_SUCCESS':
         return state.merge({
             error: 0,
             createCat: action.editCat
         });
         break;
     case 'EDIT_CAT_FAILURE':
         return state.merge({
             error: 1
         });
         break;
      default:
      return state;
 }
};
