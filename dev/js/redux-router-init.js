/* External dependencies */
import { combineReducers } from 'redux-immutable';
import Immutable from 'immutable';
import { createStore, applyMiddleware} from 'redux';
import { hashHistory, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';

/* Internal dependencies */
import appReducer from './reducers/app-reducer'
import routerReducer from './reducers/router-reducer'
import fetchCatsReducer from './reducers/advert/fetch-cats-reducer'
import createCatReducer from './reducers/advert/create-cat-reducer'
import editCatReducer from './reducers/advert/edit-cat-reducer'
////////////////////////////////////////////////

/**
 * Combine reducers into root reducer and create store.
 * Note thate 'combineReducers' is a redux-immutable version
 https://codedump.io/share/5B3ZenRoLXVz/1/redux-router-with-immutable-error
 */
const rootReducer = combineReducers({
    app: appReducer,
    routing: routerReducer,
    cats: fetchCatsReducer,
    createCat: createCatReducer,
    editCat: editCatReducer
})
const initialState = Immutable.fromJS({});

const logger = createLogger();

const store = createStore(
  rootReducer,
  initialState,
  // Enable redux dev tools
  applyMiddleware(thunk, logger, promise)
  // window.devToolsExtension &&
  // window.devToolsExtension()
);

/* Create enhanced history object for router */
const createSelectLocationState = () => {
  let prevRoutingState, prevRoutingStateJS;
  return (state) => {
    const routingState = state.get('routing'); // or state.routing
    if (typeof prevRoutingState === 'undefined' || prevRoutingState !== routingState) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }
    return prevRoutingStateJS;
  };
};

// simular browser history
const history = syncHistoryWithStore(hashHistory, store, {
  selectLocationState: createSelectLocationState()
});

////////////////////////////////////////////////

/* Exports */
export { store, history }
