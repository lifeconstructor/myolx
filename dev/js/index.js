/* External dependencies */
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute } from 'react-router'

/* Internal dependencies */
import {store, history} from './redux-router-init.js'
import {App, Home, AdvertCat, AdvertItem, Graphs} from './containers'

//////////////////////////////////////////////////

const root = document.createElement('div')
document.body.appendChild(root)

// A router with enhanced history nested in an immutable, location-aware
// redux state provider
render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="/advertCat" component={AdvertCat}/>
        <Route path="/advertItem" component={AdvertItem}/>
        <Route path="/graphs" component={Graphs}/>
      </Route>
    </Router>
  </Provider>,
  root
)
