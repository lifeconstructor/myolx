import React, {Component} from 'react';
import {connect} from 'react-redux';
import UserList from './user-list';
import UserDetails from './user-detail';

class User extends Component {
    render() {
        return (
    <div>
        <h2>User List</h2>
        <UserList />
        <hr />
        <UserDetails />
    </div>
        );
    }
}


export default User;
