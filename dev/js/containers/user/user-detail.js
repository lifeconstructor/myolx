import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {editUser} from '../../actions/index'

/*
 * We need "if(!this.props.user)" because we set state to null by default
 * */

class UserDetail extends Component {
    render() {
        if (this.props.user.size == 0) {
            return (<div>Select a user...</div>);
        }
        return (
            <div>
                <h2>User Details <small
                  onClick={() => this.props.editUser(this.props.user)}
                >[Edit]</small>
                </h2>
                <img src={this.props.user.thumbnail} />
                <h2>{this.props.user.first} {this.props.user.last}</h2>
                <h3>Age: {this.props.user.age}</h3>
                <h3>Description: {this.props.user.description}</h3>
            </div>
        );
    }
}

// "state.activeUser" is set in reducers/index.js
function mapStateToProps(state) {
    return {
        user: state.get('activeUser')
    };
}

// Get actions and pass them as props to to UserList
//      > now UserList has this.props.selectUser
function matchDispatchToProps(dispatch){
    return bindActionCreators({editUser: editUser}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(UserDetail);
