import React from 'react'
import {Link} from 'react-router'
var Globals = require('../globals.js');

export default class extends React.Component {

  men () {
    Globals.men2();
  }

  render () {
    return <div>

        <header className="product-f-header">
            <div className="container-fluid">
                <div className="pull-left">
                    <a href="#" className="product-f-logo">
                        <img src="./i/myolx.png" alt="icon-logo" />
                    </a>
                </div>
                <div className="pull-right text-right">
                    <ul className="product-flow-menu">
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/advertCat">Advert-Categories</Link></li>
                        <li><Link to="/graphs">Graphs</Link></li>
                    </ul>
                    <div onClick={() => this.men()} className="toggle-menu tm-set-1">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </header>

        <div className="admin-flow-container clearfix" style={{height: 3800}}>

            <div className="container">
                <div className="row admin-second-row-header">
                    <div className="col-xs-12 font-blue-18">Revenue</div>
                </div>
            </div>

            <div className="container">

                <div className="row">
                    <div className="col-sm-4 total">$8,328.62</div>
                    <div className="col-sm-4 search"><input type="text" placeholder="Search" /></div>
                    <div className="col-sm-4">
                        <div className="row right-menu">
                            <div><img src="i/admin-icons/blue-plus-icon.svg" /></div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12">
                        <div className="row column-names admin-third-row-header">
                            <div className="col-sm-6 col-md-3"></div>
                            <div className="col-sm-6 col-md-4">
                                <div className="row">
                                    <div className="col-xs-4 col-published">Published&nbsp;Date</div>
                                    <div className="col-xs-5 col-total">Total&nbsp;Revenue</div>
                                    <div className="col-xs-3 col-status">Status</div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-md-3 list-under col-stores">
                                <div className="row">
                                    <div className="col-xs-11 col-stores">Description</div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container list-container">

                <div className="row list-row">
                    <div className="col-sm-6 col-md-3">
                        <div className="row">
                            <div className="col-xs-4 col-sm-5 col-md-4 v-middle">
                                <div><img className="user-icon" src="i/admin-icons/empty-user-icon.svg" /></div>
                            </div>
                            <div className="col-xs-8  col-sm-7 col-md-7 no-gutter v-middle">
                                <div className="font-blue-20 v-middle">Album Title</div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-4 list-under">
                        <div className="row">
                            <div className="col-xs-5 col-sm-5 font-gray-16 v-middle">
                                <div className="v-middle">August 2015</div>
                            </div>
                            <div className="col-xs-4 col-sm-5 font-black-16 v-middle sum-parent">
                                <div className="v-middle">$3,000.00</div>
                            </div>
                            <div className="col-xs-3 col-sm-2 v-middle status-icon-col">
                                <div><div className="little-icon green-icon"></div></div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-7 col-md-3 list-under">
                        <div className="row">
                            <div className="col-xs-12 v-middle">
                                <div>Description</div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-5 col-md-2 list-under">
                        <div className="row">
                            <div className="col-xs-12 v-middle">
                                <div><div className="big-icon gray-icon" style={{float:'right'}}>view</div></div>
                                <div><div className="big-icon green-icon" style={{float:'right'}}>edit</div></div>
                                <div><div className="big-icon red-icon" style={{float:'right'}}>delete</div></div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>


        </div>
    </div>;
  }
}
