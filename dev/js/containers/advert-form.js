import React, {Component, PropTypes} from 'react';

class AdvertForm extends React.Component {

  constructor(props) {
    super(props);
    // this.closeForm = this.closeForm.bind(this);
    // this.getForm = this.getForm.bind(this);
    this.handleEditName = this.handleEditName.bind(this);
    this.handleEditDesc = this.handleEditDesc.bind(this);

    this.state = {
      editName: (this.props.formFields!=undefined && this.props.formFields.name!='')
                  ? this.props.formFields.name : '',
      editDesc: (this.props.formFields!=undefined && this.props.formFields.desc!='')
                  ? this.props.formFields.desc : '',
    };
  }

  closeForm(){
    this.props.closeForm();
  }

  getForm(){
    let catName = this.refs.Name.value;
    let catDesc = this.refs.Desc.value;
    return {'name': catName, 'desc': catDesc};
  }
  createCat(){
    this.props.createCat(this.getForm());
    this.closeForm();
  }
  editCat(){
    this.props.editCat(this.props.formFields.id, this.getForm());
    this.closeForm();
  }
  handleEditName(event){
    this.setState({
      editName: event.target.value
    });
  }
  handleEditDesc(){
    this.setState({
      editDesc: event.target.value
    });
  }

  render() {
    // inputName (read only - if no "onChange" attr)
    let inputName = (this.props.formName=='Create')
    ?
      <input type="text" ref="Name" placeholder="Name" />
    :
      <input onChange={this.handleEditName} type="text" ref="Name" placeholder="Name"
        value={this.state.editName} />

    // inputDesc (read only - if no "onChange" attr)
    let inputDesc = (this.props.formName=='Create')
    ?
      <input type="text" ref="Desc" placeholder="Desc" />
    :
      <input onChange={this.handleEditDesc} type="text" ref="Desc" placeholder="Desc"
        value={this.state.editDesc} />

    // return
    return(<div className="get-in-touch">
        <div className="gt-form">
            <div className="gt-heading">{this.props.formName} Category</div>

            {inputName}

            {inputDesc}

            <select className="selectbox" data-placeholder="topic">
                <option>Topic 1</option>
                <option>Topic 2</option>
                <option>Topic 3</option>
            </select>
            <input type="text" placeholder="Message" />

            <button
              type="submit"
              className="btn-shape btn-ship-it"
              onClick={
                () => (this.props.formName=='Create') ?
                  this.createCat() :
                    (this.props.formName=='Edit') ?
                        this.editCat() :
                        this.closeForm()
                }>
                Save
            </button>
        </div>
    </div>);
  }
}

export {AdvertForm};
