import React from 'react'
import {Link} from 'react-router'

export default class extends React.Component {
  // list all pages in parent - <Route />
  render () {
    return (<div> { this.props.children } </div>);
  }
}
