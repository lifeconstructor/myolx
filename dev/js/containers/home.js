import React from 'react'
import {Link} from 'react-router'

export default class extends React.Component {
  render () {
    return <div className="product-flow-container clearfix">
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <div className="flow-sidebar">
                            <p className="flow-subheading">Navigation</p>
                            <div className="menu-toggle-icon">
                                <div className="toggle-menu">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                            <ul className="menu-flow">
                                <li className="active"><Link to="/advertCat">Categories</Link></li>
                                <li className="active"><Link to="/advertItem">Advertsment Items</Link></li>
                                <li className="active"><Link to="/graphs">Graphs</Link></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-9 col-md-9-contaier">
                        <div className="row-product-flow">
                            <div className="product-breadcrumbs">
                                <div className="b-table">
                                    <div className="b-table-cell breadcrumbs-cell clearfix">
                                        <ul className="product-breadcrumbs-list">
                                            <li><a href="#">MyOLX  </a></li>
                                            <li><a href="#">Navigation</a></li>
                                        </ul>

                                        <div className="breadcrumb-right">
                                            <a href="#"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row-product-flow">
                            <div className="flow-content">
                                <form action="#">
                                    <input type="text" className="pf-input" placeholder="Email Address" />
                                    <input type="password" className="pf-input" placeholder="Password" />

                                    <button type="submit" className="button-set-5">Login</button>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>;
  }
}

// TODO: jquery for Home menu (could be in the wrong place)
$(function(){
  // Menu
  var text = $('.toggle-menu > li.selected').text();
  $('.toggle-selected-item').html(text);

  $('.collapse-nav-heading').click(function () {
      $('.collapse').collapse('toggle');
  });

  $('.toggle-selected-item, .menu-open-close').click(function () {
      $('.toggle-menu-block').toggleBox('active');
  });

  $('.toggle-menu > li').on('click', function () {
      var $that = $(this);
      var text = $that.text();
      $('.toggle-menu > li').removeClass('selected');
      $that.addClass('selected');
      $('.toggle-selected-item').html(text);
      $('.toggle-menu-block').toggleBox('active');
  });

  $.fn.toggleBox = function ($class) {
      $this = $(this);
      if ($this.hasClass($class)) {
          $this.removeClass($class);
      }
      else {
          $this.addClass($class);
      }
  };

  $('.menu-toggle-icon').click(function () {
      var $this = $(this);
      $this.parent().find('.menu-flow').slideToggle();
  });

  /**
   |--------------------------------------------------------------------------
   | Smooth scroll
   |--------------------------------------------------------------------------
   */
  $('a[href*=#]:not([href=#])').click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
              $('html,body').animate({
                  scrollTop: target.offset().top
              }, 1000);
              //return false;
          }
      }
  });


  // Admin (TODO: seperate to admin.js)
  $(".main-list-block").click(function () {
      $(".bottom-list").slideToggle();
  });

});
