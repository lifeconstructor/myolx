import React from 'react'
import {Link} from 'react-router'
var Globals = require('../globals.js');

export default class extends React.Component {

  componentDidMount(){
    jqueryGraphics();
  }

  men () {
    Globals.men2();
  }

  render () {
    return <div>

        <header className="product-f-header">
            <div className="container-fluid">
                <div className="pull-left">
                    <a href="#" className="product-f-logo">
                        <img src="./i/myolx.png" alt="icon-logo" />
                    </a>
                </div>
                <div className="pull-right text-right">
                    <ul className="product-flow-menu">
                      <li><Link to="/">Home</Link></li>
                      <li><Link to="/advertCat">Advert-Categories</Link></li>
                      <li><Link to="/advertItem">Advert-Items</Link></li>
                    </ul>
                    <div onClick={() => this.men()} className="toggle-menu tm-set-1">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </header>

        <div className="admin-flow-container clearfix" style={{height: 1800}}>

            <div className="container">
                <div className="row admin-second-row-header">
                    <div className="col-xs-12 font-blue-18">Category Revenue</div>
                </div>
                <div className="row">
                    <div className="col-sm-4 total">Dashboard</div>
                    <div className="col-sm-8">
                        <div className="row right-menu">
                            <div className="font-blue-16">Add New Advertisment <img src="i/admin-icons/blue-plus-icon.svg" alt="plus-icon" className="plus-icon" /></div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-md-9">
                        <div className="col-xs-12 canvas-container">
                            <div className="row">
                                <div className="col-xs-12 canvas-header">
                                    <div className="row">
                                        <div className="col-xs-4 col-md-8">Graph Name</div>
                                        <div className="col-xs-4 col-md-2">Yearly <img src="i/admin-icons/gray-arrow-dropdown.svg" alt="gray-arrow-dropdown" /></div>
                                        <div className="col-xs-4 col-md-2">All Types <img src="i/admin-icons/gray-arrow-dropdown.svg" alt="gray-arrow-dropdown" /></div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 summ-canvas">
                                    <span>$8,328.62</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <canvas id="myCanvas" width="1600" height="336" style={{width: '100%'}}>
                                        Your browser does not support the HTML5 canvas tag.</canvas>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 canvas-header">
                                    <div className="row">
                                        <div className="col-xs-1 month">January</div>
                                        <div className="col-xs-1 month">February</div>
                                        <div className="col-xs-1 month">March</div>
                                        <div className="col-xs-1 month">April</div>
                                        <div className="col-xs-1 month">May</div>
                                        <div className="col-xs-1 month">June</div>
                                        <div className="col-xs-1 month">July</div>
                                        <div className="col-xs-1 month">August</div>
                                        <div className="col-xs-1 month">September</div>
                                        <div className="col-xs-1 month">October</div>
                                        <div className="col-xs-1 month">November</div>
                                        <div className="col-xs-1 month">December</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                        </div>

                        <div className="col-xs-12 wide-table-line">

                                            <div className="row">
                                                <div className="col-xs-3 v-middle">
                                                    <div>
                                                        <img className="user-icon" src="i/admin-icons/empty-user-icon.svg" alt="user-icon" />
                                                    </div>
                                                </div>
                                                <div className="col-xs-9 v-middle">
                                                    <div className="font-blue-20">Go State Go</div>
                                                </div>
                                            </div>


                        </div>
                    </div>

                    <div className="col-xs-12 col-md-3">
                        <div className="col-xs-12 col-md-12">
                            <div className="row">
                                <div className="col-xs-12 block-number-right">
                                    <img src="i/admin-icons/green-arrow-up.svg" alt="green-arrow" />
                                    <div className="big-number">2</div>
                                    <div className="font-blue-22 font-size-18">Total Items</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>;
  }
}

// TODO: (could be in the wrong place)
function jqueryGraphics(){
  var c = document.getElementById("myCanvas");
  if (c != null){
      var ctx = c.getContext("2d");

      // bg-lines
      ctx.beginPath();
      ctx.moveTo(0, 0);
      ctx.lineTo(1620, 0);// line end
      ctx.moveTo(0, 56);
      ctx.lineTo(1620, 56);// line end
      ctx.moveTo(0, 112);
      ctx.lineTo(1620, 112);// line end
      ctx.moveTo(0, 168);
      ctx.lineTo(1620, 168);// line end
      ctx.moveTo(0, 224);
      ctx.lineTo(1620, 224);// line end
      ctx.moveTo(0, 280);
      ctx.lineTo(1620, 280);// line end
      ctx.moveTo(0, 336);
      ctx.lineTo(1620, 336);// line end
      ctx.lineWidth = 2;
      ctx.strokeStyle = "#8FA3AD";
      ctx.lineCap = "butt";
      ctx.stroke();

      // Blue-line (NOTE: repeat in loop for different A-B points for "Blue-line Graph")
      // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineTo
      var A = {'left':0, 'top':325};
      var B = {'left':1280, 'top':100};

      ctx.fillStyle = 'red';
      ctx.fillRect(A.left, A.top, 10, 10);// START Red-point
      ctx.beginPath();
      ctx.moveTo(A.left, A.top);// start line
      ctx.lineTo(B.left, B.top);// end line
      ctx.fillRect(B.left, B.top, 10, 10);// END Red-point
      ctx.lineWidth = 6;
      ctx.strokeStyle = "blue";
      ctx.stroke();

      // Lable
//        ctx.beginPath();
//        ctx.moveTo(1060, 140);
//        ctx.lineTo(1160, 140);
//        ctx.lineWidth = 50;
//        ctx.strokeStyle = "#8FA3AD";
//        ctx.lineCap = "round";
//        ctx.stroke();
//        ctx.font = '14pt Calibri';
//        ctx.fillStyle = 'white';
//        ctx.fillText("Name Of Lable", 1050, 146);
  }
}
