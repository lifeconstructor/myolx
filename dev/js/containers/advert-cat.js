import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router'
import { connect } from 'react-redux';
var Globals = require('../globals.js');// some ajax
import {AdvertForm} from './advert-form';
import {ModalContainer, ModalDialog} from 'react-modal-dialog';
import {advertCatsAction} from '../actions/advert-cats-action';
import {deleteCatAction} from '../actions/delete-cat-action';
import {createCatAction} from '../actions/create-cat-action';
import {editCatAction} from '../actions/edit-cat-action';
import {filterCatsAction} from '../actions/filter-cats-action';


import Immutable from 'immutable';

var catFilterType = '';
var catFilterValue = '';

// Componet
class AdvertCat extends Component {

  constructor(props) {
    super(props);

    // initial state
    this.state = {
      isShowingModal: false,
      isShowingModalCreate: false,
      catId: 0,
      valueFilterName: '',
      valueFilterDesc: '',
      filterType: '',
      filterValue: ''
    };

    // modal handlers
    this.handleEdit   = this.handleEdit.bind(this);
    this.handleClose  = this.handleClose.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    // delete action
    this.deleteCatAction = deleteCatAction.bind(this);
    // create handler
    this.createCat = this.createCat.bind(this);
    // edit action
    this.editCat = this.editCat.bind(this);
    // filters handlers
    this.handleFilterName = this.handleFilterName.bind(this);
    this.handleFilterDesc = this.handleFilterDesc.bind(this);
    // filter action
    this.filterCatsAction = filterCatsAction.bind(this);
  }

  // CRUD handlers
  handleEdit (catId) {
    this.setState({isShowingModal: true, catId: catId})
  }
  handleClose () {
    this.setState({
      isShowingModal: false,
      isShowingModalCreate: false,
    })
  }
  handleDelete (catId) {
    let that = this;
    let r = confirm("Do you really want to delete this item ?");
    if (r == true) {
        // delete catId item
        Promise.resolve(this.deleteCatAction(catId)) // dispatch
        .then(function (response) {
          // re-list categories
          that.props.advertCatsAction(); //dispatch
          return response;
        });
    }
  }
  handleCreate () {
    this.setState({isShowingModalCreate: true});

  }
  createCat(postingData){
    //alert(postingData);
    let that = this;
    Promise.resolve(this.props.createCatAction(postingData)) // dispatch
    .then(function (response) {
      that.props.advertCatsAction(); //dispatch
      return response;
    });
  }
  editCat(catId, postingData){
    //alert(postingData);
    let that = this;

    Promise.resolve(this.props.editCatAction(catId, postingData)) // dispatch
    .then(function (response) {
      that.props.advertCatsAction(); //dispatch
      return response;
    });
  }

  // Filter handlers
  handleFilterName(event){ // Name
    this.setState({
      valueFilterName: event.target.value,
    });
    this.props.filterCatsAction(
      this.props.dispatch,
      catFilterType = 'Name',
      catFilterValue = event.target.value
    );
  }
  handleFilterDesc(event){ // Desc
    this.setState({
      valueFilterDesc: event.target.value,
    });
    this.props.filterCatsAction(
      this.props.dispatch,
      catFilterType = 'Desc',
      catFilterValue = event.target.value
    );
  }

  componentWillMount(){
    // fetch cats
    this.props.advertCatsAction();
  }

  // handle top menu
  menu () {
    Globals.men2();
  }

  // Advert Cat list
  renderList() {
    return this.props.cats.get('cats').map((cat, k) => {
        return (
          <div className="row list-row" key={cat.get('id')}>
              <div className="col-sm-5 col-md-5">
                  <div className="row">
                      <div className="col-xs-4 col-sm-5 col-md-4 v-middle">
                          <div><img className="user-icon" src="i/admin-icons/empty-user-icon.svg" /></div>
                      </div>
                      <div className="col-xs-8  col-sm-7 col-md-8 no-gutter v-middle">
                          <div className="font-blue-20 v-middle">{cat.get('name')}</div>
                      </div>
                  </div>
              </div>
              <div className="col-sm-7 col-md-7">
                  <div className="row">
                      <div className="col-xs-12 col-md-6 font-gray-16 v-middle">
                          <div className="v-middle">{cat.get('desc')}</div>
                      </div>
                      <div className="col-xs-6 col-md-2 v-middle">
                          <div><img className="graph-i round-icon" src="i/admin-icons/admin-graph.svg" /></div>
                      </div>
                      <div className="col-xs-6 col-md-4">
                          <div className="row">
                              <div className="col-xs-12 v-middle">
                                  <div><div className="big-icon gray-icon">view</div></div>
                                  <div onClick={() => this.handleEdit(cat.get('id'))}>
                                    {
                                      this.state.isShowingModal &&
                                      this.state.catId===cat.get('id') &&
                                      <ModalContainer onClose={this.handleClose}>
                                        <ModalDialog onClose={this.handleClose}>
                                          <AdvertForm
                                              formName="Edit"
                                              formFields={{
                                                id: cat.get('id'),
                                                name: cat.get('name'),
                                                desc: cat.get('desc')
                                              }}
                                              closeForm={this.handleClose}
                                              editCat={this.editCat}

                                          />
                                        </ModalDialog>
                                      </ModalContainer>
                                    }
                                    <div className="big-icon green-icon">edit</div>
                                  </div>
                                  <div onClick={() => this.handleDelete(cat.get('id'))}>
                                    <div className="big-icon red-icon">delete</div>
                                  </div>
                              </div>
                          </div>
                      </div>

                  </div>
              </div>
          </div>

        );
    }).toArray();
  }

  // render
  render () {
    return <div>
    <header className="product-f-header">
        <div className="container-fluid">
            <div className="pull-left">
                <a href="#" className="product-f-logo">
                    <img src="./i/myolx.png" alt="icon-logo" />
                </a>
            </div>
            <div className="pull-right text-right">
                <ul className="product-flow-menu">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/advertItem">Advert-Items</Link></li>
                    <li><Link to="/graphs">Graphs</Link></li>
                </ul>
                <div onClick={() => this.menu()} className="toggle-menu tm-set-1">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </header>

    <div className="admin-flow-container clearfix" style={{height: 3800}}>

        <div className="container">

            <div className="row admin-second-row-header">
                <div className="col-xs-12 font-blue-18">{this.props.countCats} total categories</div>
            </div>
        </div>

        <div className="container">

            <div className="row">
                <div className="col-sm-3 total">Categories</div>
                <div className="col-sm-7 filter-block-1">

                        <div className="col-xs-6 marg-top-22">
                            <div className="row">
                                    <img style={{verticalAlign: 'middle !important'}} src="i/admin-icons/gray-arrow-dropdown.svg" alt="down-icon" className="down-icon" />
                                    <input onChange={
                                      this.handleFilterName
                                    } style={{marginLeft: 5}} type="text"  value={
                                      this.state.valueFilterName
                                    } placeholder="Name" />

                            </div>
                        </div>

                        <div className="col-xs-6 marg-top-22">
                            <div className="row">
                                    <img style={{verticalAlign: 'middle !important'}} src="i/admin-icons/gray-arrow-dropdown.svg" alt="down-icon" className="down-icon" />
                                    <input onChange={
                                      this.handleFilterDesc
                                    } style={{marginLeft: 5}} type="text"  value={
                                      this.state.valueFilterDesc
                                    } placeholder="Description" />

                            </div>
                        </div>

                </div>
                <div className="col-sm-2">
                    <div className="row right-menu">
                        <div onClick={(val) => this.handleCreate(val) }>
                        {
                          this.state.isShowingModalCreate &&
                          <ModalContainer onClose={this.handleClose}>
                            <ModalDialog onClose={this.handleClose}>
                              <AdvertForm
                                  formName="Create"
                                  closeForm={this.handleClose}
                                  createCat={this.createCat}
                              />
                            </ModalDialog>
                          </ModalContainer>
                        }
                          <img src="i/admin-icons/blue-plus-icon.svg" />
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div className="container list-container">

        {/* List Items */}
        {this.renderList()}

        </div>

    </div>
    </div>;
  }

}


// This function is used to convert redux global state to desired props.
function mapStateToProps(state) {
  // Map collection
  let cats = state.get('cats');
  let createCat = state.get('createCat');
  let editCat = state.get('editCat');
  return {
    // in container/component `this.props.cats`
    cats: cats,
    createCat: createCat,
    editCat: editCat,
    countCats: cats.get('cats').toArray().length

  };
}

// This function is used to provide callbacks to container component.
function mapDispatchToProps(dispatch) {
  return {
    // fetch categories
    advertCatsAction: () => advertCatsAction(dispatch),
    filterCatsAction: () => filterCatsAction(dispatch, catFilterType, catFilterValue),
    createCatAction: (postingData) => createCatAction(dispatch, postingData),
    editCatAction: (catId, postingData) => editCatAction(dispatch, catId, postingData),
  };
}
// mapStateToProps
// We are using `connect` function to wrap our component with special component, which will provide to container all needed data.
export default connect(mapStateToProps,  mapDispatchToProps)(AdvertCat);
