<?php
/* @var $this AdvertCatController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Advert Cats',
);

$this->menu=array(
	array('label'=>'Create AdvertCat', 'url'=>array('create')),
	array('label'=>'Manage AdvertCat', 'url'=>array('admin')),
);
?>

<h1>Advert Cats</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
