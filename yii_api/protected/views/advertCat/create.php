<?php
/* @var $this AdvertCatController */
/* @var $model AdvertCat */

$this->breadcrumbs=array(
	'Advert Cats'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdvertCat', 'url'=>array('index')),
	array('label'=>'Manage AdvertCat', 'url'=>array('admin')),
);
?>

<h1>Create AdvertCat</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>