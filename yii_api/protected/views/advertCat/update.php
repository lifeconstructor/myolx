<?php
/* @var $this AdvertCatController */
/* @var $model AdvertCat */

$this->breadcrumbs=array(
	'Advert Cats'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdvertCat', 'url'=>array('index')),
	array('label'=>'Create AdvertCat', 'url'=>array('create')),
	array('label'=>'View AdvertCat', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdvertCat', 'url'=>array('admin')),
);
?>

<h1>Update AdvertCat <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>