<?php
/* @var $this AdvertCatController */
/* @var $model AdvertCat */

$this->breadcrumbs=array(
	'Advert Cats'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List AdvertCat', 'url'=>array('index')),
	array('label'=>'Create AdvertCat', 'url'=>array('create')),
	array('label'=>'Update AdvertCat', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdvertCat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdvertCat', 'url'=>array('admin')),
);
?>

<h1>View AdvertCat #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'desc',
	),
)); ?>
