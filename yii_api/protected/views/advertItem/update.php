<?php
/* @var $this AdvertItemController */
/* @var $model AdvertItem */

$this->breadcrumbs=array(
	'Advert Items'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdvertItem', 'url'=>array('index')),
	array('label'=>'Create AdvertItem', 'url'=>array('create')),
	array('label'=>'View AdvertItem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdvertItem', 'url'=>array('admin')),
);
?>

<h1>Update AdvertItem <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'aAdvertIdCategories'=>$this->aAdvertIdCategories
        )); ?>