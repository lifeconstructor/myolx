<?php
/* @var $this AdvertItemController */
/* @var $model AdvertItem */

$this->breadcrumbs=array(
	'Advert Items'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AdvertItem', 'url'=>array('index')),
	array('label'=>'Create AdvertItem', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#advert-item-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Advert Items</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'advert-item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'id',
                    'header'=>'ID',
                    "type" => "raw",
                    'value'=>function($data){
                        print $data->id;
                    },
                    'htmlOptions'=>array('width'=>'20px','text-align'=>'center'),
                ),
                array(
                    'name'=>'catName',
                    'header'=>'Category',
                    "type" => "raw",
                    //'value'=>array($this,'gridCategory'),
                    'value'=>function($data){
                        print $data->itemCat->name;
                    },
                    'htmlOptions'=>array('width'=>'70px','text-align'=>'center'),
                ),
		'name',
		'description',
		'price',
		'date_created',
		'date_edited',
		/*
		'cat_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
