<?php
/* @var $this AdvertItemController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Advert Items',
);

$this->menu=array(
	array('label'=>'Create AdvertItem', 'url'=>array('create')),
	array('label'=>'Manage AdvertItem', 'url'=>array('admin')),
);
?>

<h1>Advert Items</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
