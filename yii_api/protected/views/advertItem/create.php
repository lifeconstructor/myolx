<?php
/* @var $this AdvertItemController */
/* @var $model AdvertItem */

$this->breadcrumbs=array(
	'Advert Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdvertItem', 'url'=>array('index')),
	array('label'=>'Manage AdvertItem', 'url'=>array('admin')),
);
?>

<h1>Create AdvertItem</h1>

<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'aAdvertIdCategories'=>$this->aAdvertIdCategories
        )); ?>