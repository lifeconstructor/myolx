<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>


<?php if(Yii::app()->user->isGuest){?>
    <p>This is Backend for "My OLX"</p>
    
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rhoncus arcu vitae ipsum eleifend semper. Vivamus malesuada arcu in mauris lobortis posuere. Cras congue ac dui ac imperdiet. Nullam quis purus erat. Curabitur suscipit at nisl sed porta. Nam non sapien quis justo molestie dignissim. Donec eu turpis id leo fermentum tristique. Praesent placerat lacus blandit aliquet laoreet.</p>

    <p>Praesent efficitur id diam quis fermentum. Maecenas sit amet nisi magna. Nam id malesuada augue, ut gravida massa. Vivamus auctor lobortis quam, ornare commodo ligula dignissim a. Etiam ultrices sed sapien ac gravida. Donec eu vulputate nisl, vel congue diam. Fusce vehicula egestas pulvinar. Ut placerat maximus metus sed elementum. Integer vel mollis odio. Curabitur ut dapibus quam. Curabitur et ex in lectus ultricies molestie vitae ac ligula. Pellentesque vulputate arcu vel ex finibus, non volutpat libero volutpat.</p>

    <p>Morbi in volutpat velit. Nunc quis mauris vitae felis dictum varius. Pellentesque condimentum urna vitae sollicitudin auctor. Curabitur dignissim commodo purus vel pretium. Vivamus in luctus tellus. Phasellus commodo mi nec euismod congue. Nulla vestibulum vel quam quis gravida. Sed ultrices urna a arcu volutpat luctus. Nunc sed ultrices nibh. Vivamus aliquet sagittis ex, id accumsan eros dignissim id. Donec pellentesque in ante quis ullamcorper. Nunc rutrum iaculis justo, nec scelerisque tortor ullamcorper vel. Sed non convallis nulla.</p>
<?php }else{?>
    <h4>Project Description:</h4>
    
    <p>Создать простенькую доску обьявлений. (такой себе маленький OLX)</p>

    <p>
        <b>
            * Создание, редактирование, удаление обьявлений <br>
            * Каждое обьявление должно иметь набор характеристик и категорий. <br>
            * Фильтрация и сортировка по различным признакам - (имя, дата, характеристика, ...) <br>
            * Прикрутить график с ипсользованием D3 (к примеру статистика обьявлений по категориям или чтото подобное) с динамическим обновлением в зависимости от условий. <br>
            * Должны присутствовать минимально тесты, каменты в коде и т.д.(Все на англ. языке) <br>
        </b>
    </p><p>
        Технологии: <br>
        - React + желательно Redux (на крайний случай Reflux), <br>
        - Underscore, <br>
        - Bootstrap(стилистика не обязательна, но нам нужно увидеть знания по CSS и CSS3, + старницы дожны быть минимально респонсив), <br>
        - Использовать LocalStorage,<br>
        - jQuery стараться использовать только для AJAX ну или не использовать вообще,<br>
        - PHP (желательно Slim Application - но не критично),<br>
        - MySQL<br>
    </p><p>
        Можно прикрутыть ещё какуюто загрузку имеджей.<br>
    </p><p>
        Залить всё на Git (включая срез базы и readme с кратким описание возможностей и инструкцией для установки как для нубов на АНГЛ. языке)</p>
    </p>
    
    <?php } ?>
