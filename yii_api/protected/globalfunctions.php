<?php

// return current controller
function c(){ 
    return (Yii::app()->controller->id)?Yii::app()->controller->id:var_dump(Yii::app()->controller->id); 
}
// return current action
function a(){ 
    return Yii::app()->controller->action->id; 
}
// return current controller alias
function cAlias(){ 
    $aUrl = explode('/',$_SERVER['REQUEST_URI']);
    return ($aUrl[1]=='backend.php')? $aUrl[2] : $aUrl[1];
}
// return current action alias
function aAlias(){ 
    $aUrl = explode('/',$_SERVER['REQUEST_URI']);
    return ($aUrl[1]=='backend.php')? $aUrl[3] : $aUrl[2];
}


// Show time difference
// return (array)
function timeDiff($date, $date2 = 0)
{
    if(!$date2)
        $date2 = time();

    $date_diff = array('seconds'  => '',
                       'minutes'  => '',
                       'hours'    => '',
                       'days'     => '',
                       'weeks'    => '',
                       
                       'tseconds' => '',
                       'tminutes' => '',
                       'thours'   => '',
                       'tdays'    => '',
                       'tdays'    => '');

    ////////////////////
    
    if($date2 > $date)
        $tmp = $date2 - $date;
    else
        $tmp = $date - $date2;

    $seconds = $tmp;

    // Relative ////////
    $date_diff['weeks'] = floor($tmp/604800);
    $tmp -= $date_diff['weeks'] * 604800;

    $date_diff['days'] = floor($tmp/86400);
    $tmp -= $date_diff['days'] * 86400;

    $date_diff['hours'] = floor($tmp/3600);
    $tmp -= $date_diff['hours'] * 3600;

    $date_diff['minutes'] = floor($tmp/60);
    $tmp -= $date_diff['minutes'] * 60;

    $date_diff['seconds'] = $tmp;
    
    // Total ///////////
    $date_diff['tweeks'] = floor($seconds/604800);
    $date_diff['tdays'] = floor($seconds/86400);
    $date_diff['thours'] = floor($seconds/3600);
    $date_diff['tminutes'] = floor($seconds/60);
    $date_diff['tseconds'] = $seconds;

    return $date_diff;
}


// ------------------------------------------------------------ XML handling
function array_to_xml(array $arr, SimpleXMLElement $xml){// convert
    foreach ($arr as $k => $v) 
        is_array($v)? array_to_xml($v, $xml->addChild($k)) : $xml->$k = $v;//$xml->addChild($k, $v);
    return $xml;
}
function array_to_xmlArray($objArr, &$xmlArr){// set values
    $aReturn = array();// array stack
    foreach ($xmlArr as $k => &$v) {
        if(is_array($v)){
            array_to_xmlArray($objArr, $v, $aReturn);
        } else {
            //print_r($objArr);exit;
            // Two values in the tag
            if(substr_count($v, '{')==2){
                $aV = explode(' ',$v);
                $vvv = '';
                foreach($aV as $k=>$vv){
                    $draftKey = substr($vv,1);
                    $key = substr($draftKey,0,(strlen($draftKey)-1));
                    if(isset($objArr[$key])) {
                        if(!$k){
                            $vvv = $objArr[$key];
                        }else{ 
                            if(strpos($vvv, $objArr[$key])){
                                $v = $vvv;
                            }else{
                                $v = $vvv.' '.$objArr[$key];
                            }
                        }
                    }         
                }
            }else if(substr($v,0,1)=='{'){// One value in the tag
                $draftKey = substr($v,1);
                $key = substr($draftKey,0,(strlen($draftKey)-1));
                if(isset($objArr[$key])) {
                    $v = $objArr[$key];
                }
                if($k=='phone' || $k=='phone2' || $k=='phone-2' || $k=='phone_2'){
                if(isset($objArr[$k])) {
                    $v = $objArr[$k];
                }
                }
            }
        }
    }
}
function xml_to_array($data){// convert
    $result = array();
    foreach ($data as $key => $value)
        if(is_object($value)) {
            if(!sizeof($value)){
                $a = (array)$value[0];
                $result[$key] = $a[0];
            }else
                $result[$key] = xml_to_array($value);
        }
    return $result;
}

/*******************************************************************************
 * FUNCTION FOR TESTING using 8 arguments, to show vars in Array and exit();
 * @param Array|String $arrayORstr
 * @param string $some1, $some2, $some3
 * @return Array
 *******************************************************************************/
function tt($arrayORstr, $some1='',$some2='',$some3='') {
    if (is_array($arrayORstr) && (!$some1 && !$some2 && !$some3)) {
        print_r(array($arrayORstr));exit;
    } else {
        $a = array($arrayORstr, $some1, $some2, $some3);
        $b = array();
        foreach ($a as $v) $b[] = $v;
        print_r($b);exit;
    }
}
// var_dump() version
function ttv($arrayORstr, $some1='',$some2='',$some3='') {
    if (is_array($arrayORstr) && (!$some1 && !$some2 && !$some3)) {
        var_dump(array($arrayORstr));exit;
    } else {
        $a = array($arrayORstr, $some1, $some2, $some3);
        $b = array();
        foreach ($a as $v) $b[] = $v;
        var_dump($b);exit;
    }
}
?>
