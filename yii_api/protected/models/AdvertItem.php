<?php

/**
 * This is the model class for table "advert_item".
 *
 * The followings are the available columns in table 'advert_item':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property integer $price
 * @property string $date_created
 * @property string $date_edited
 * @property integer $cat_id
 */
class AdvertItem extends CActiveRecord
{
        public $catName;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'advert_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, price, cat_id', 'required'),
			array('price', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>250),
                        array('name','unique', 'message'=>'This name already exists.'),
			array('description', 'length', 'max'=>1024),
                    array('id, name, description, price, date_created, date_edited, cat_id, catName', 'safe', 'on' => 'rest'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, price, date_created, date_edited, cat_id, catName', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'itemCat' => array(self::BELONGS_TO, 'AdvertCat', 'cat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'price' => 'Price',
			'date_created' => 'Date Created',
			'date_edited' => 'Date Edited',
			'cat_id' => 'Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = "itemCat";

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_edited',$this->date_edited,true);
                
		//$criteria->compare('cat_id',$this->cat_id);
                
                // Here is an example how to filter by Relation
                // http://www.mrsoundless.com/php/yii/searching-and-sorting-a-column-from-a-related-table-in-a-cgridview/
                $criteria->compare('itemCat.name', $this->catName,true);

                
                $sort = new CSort();
                $sort->attributes = array(
                    'defaultOrder'=>'t.id DESC',
                    'catName'=>'itemCat.name DESC'
                );
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>$sort
                        
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdvertItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
