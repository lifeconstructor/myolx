<?php

// class ERestController        - (Docs) https://github.com/evan108108/RESTFullYii/tree/v1.15
// class AdvertItemController   - CRUD for AdvertItem model

class AdvertItemController extends ERestController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    
    // AdvertCat Array('id'=>'name');
    public $aAdvertIdCategories = array();

    protected function beforeAction($event)
    {
        // get all Advert Categories
        $AC = AdvertCat::model()->findAll();
        $newArr = array();
        foreach($AC as $v)
            $newArr[$v->id] = $v->name;
        // Array('id'=>'name');
        $this->aAdvertIdCategories = $newArr;
        return true;
    }
    
    protected function gridCategory($data,$row)
    { 
            return isset($this->aAdvertIdCategories[$data->cat_id])
                            ? $this->aAdvertIdCategories[$data->cat_id]
                            : '';
    }   
    
    /**
     * @return array action filters
     */
    public function _filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function _accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view',
                     // REST Actions
                    'restView', 'restList', 'restCreate', 'restUpdate', 'restDelete'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
            'aAdvertIdCategories'=>$this->aAdvertIdCategories
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new AdvertItem;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AdvertItem'])) {
            
            $model->attributes = $_POST['AdvertItem'];
            $model->date_created = date("Y-m-d h:i:s", time());
            $model->date_edited = date("Y-m-d h:i:s", time());
            //tt($model->attributes);
            
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
            'aAdvertIdCategories'=>$this->aAdvertIdCategories
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AdvertItem'])) {
            $model->attributes = $_POST['AdvertItem'];
            $model->date_edited = date("Y-m-d h:i:s", time());

            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
            'aAdvertIdCategories'=>$this->aAdvertIdCategories
        ));
    }


    /* Start REST Actions */

    public function actionRestView($id)
    {
            $this->doRestView($id);
    }

    public function actionRestList()
    {
        $this->doRestList();
    }

    public function actionRestCreate($data)
    {
        $this->doRestCreate($data);
    }

    public function actionRestUpdate($id, $data)
    {
        $this->doRestUpdate($id, $data);
    }

    public function actionRestDelete($id)
    {
        $this->doRestDelete($id);
    }
    /* End Rest Actions */
        
    
    
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('AdvertItem');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new AdvertItem('search');
        $model->unsetAttributes();  // clear any default values
        
        if (isset($_GET['AdvertItem']))
            $model->attributes = $_GET['AdvertItem'];

        $this->render('admin', array(
            'model' => $model,
            //'aAdvertIdCategories'=>$this->aAdvertIdCategories
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return AdvertItem the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = AdvertItem::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param AdvertItem $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'advert-item-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
