<?php

// class ERestController        - (Docs) https://github.com/evan108108/RESTFullYii/tree/v1.15
// class AdvertCatController    - CRUD for AdvertCat model

class AdvertCatController extends ERestController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function _accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'create', 'delete', 'update',
                                    // REST Actions
                                    'restView', 'restList', 'restCreate', 'restUpdate', 'restDelete',
                                    'REST.GET', 'REST.PUT', 'REST.POST', 'REST.DELETE'),
				'users'=>array('*'),
			),

		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AdvertCat;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AdvertCat']))
		{
			$model->attributes=$_POST['AdvertCat'];
//                        print 'ggg';
//                        print_r($model->attributes);exit;
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AdvertCat']))
		{
			$model->attributes=$_POST['AdvertCat'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AdvertCat');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AdvertCat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AdvertCat']))
			$model->attributes=$_GET['AdvertCat'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        
        /* Override RESTfull Actions */
	public function actionRestView($id)
	{
            $this->doRestView($id);
	}
        
        public function actionRestList()
	{
            $this->doRestList();
	}

        public function actionRestCreate()
	{
            if(isset($_POST['AdvertCat'])){
                $data = $_POST['AdvertCat'];
                $this->doRestCreate($data);
            } else 
                throw new CHttpException(400, 'Not expected Request method or Yii Model.');
	}

        // DO NOT Override THIS METHOD
//        public function actionRestUpdate($id)
//	{
//            //tt($id, $_REQUEST, file_get_contents('php://input'));
//            $this->doRestUpdate($id, null);
//	}

        public function actionRestDelete($id)
	{
            $this->doRestDelete($id);
	}
        /* End Rest Actions */
        
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AdvertCat the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AdvertCat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AdvertCat $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='advert-cat-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
