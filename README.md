MyOLX - Version 1.0

* CRUD and Filter categories
* Each advertisement have a set of characteristics and categories
* Filtering and sorting by various attributes - (name, date, characteristics, ...
* Link to graphics (for example statistics about advertisements by category) with dynamic JS update, depending on conditions

Technologies used:
- React/Redux, routing
- JS Collections,
- Bootstrap, Responsive design
- * LocalStorage,
- PHP (ready Yii framework for REST Api in root directory)
- MySql

INSTALLATION

-------------------------------- API SERVER ---------------------------

1. I used "xampp" for http://localhost:8080
1. Download latest Yii (1)
    * you can use any php framework for API, but project is using yii on http://localhost:8080
2. Set "httpd-vhosts":
    <VirtualHost 127.0.0.1:80>
    	ServerAdmin orest.b2b@gmail.com
    	DocumentRoot "C:\xampp\htdocs\myolx"
    	ServerName myolx
    	ServerAlias myolx
    	Header set Access-Control-Allow-Origin "http://localhost:3000"
    	Header set Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"
      Header set Access-Control-Allow-Headers "HTTP_X_REST_USERNAME, HTTP_X_REST_PASSWORD"
    </VirtualHost>
3. place "yii" folder in the same directory where "myolx" is
4. place "yii_api" (git) project in "myolx"
5. import DB dump for project
6. Change credentials at ../protected/config/database.php
7. Launch appache and Visit http://localhost:8080

DB Dump:
CREATE TABLE IF NOT EXISTS `advert_cat` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `desc` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `advert_cat` (`id`, `name`, `desc`) VALUES
(1, 'TVs', 'LCD, Plasma'),
(2, 'Computers', 'PC, Laptop, Netbook, Touchpad, Mac');

CREATE TABLE IF NOT EXISTS `advert_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `price` int(10) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_edited` datetime NOT NULL,
  `cat_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `advert_item` (`id`, `name`, `description`, `price`, `date_created`, `date_edited`, `cat_id`) VALUES
(1, 'sefy', 'fghfghd erthe', 10, '2016-10-09 04:13:11', '2016-10-09 08:25:29', 1),
(2, 'ggg', 'qqq', 555, '2016-10-09 04:27:54', '2016-10-09 07:26:05', 2);


-------------------------------- NODE.Js SERVER -----------------------

1. Download project to your "nodeproject" directory
2. npm start
3. Change "..dev/js/constants.js" for your API localhost
4. Visit: http://localhost:3000


DEVELOPMRNT DOCS

--------------------- API server (Apache/PHP/Yii) -----------------------

* Login: admin, admin
* Most Docs available at - ../site/docs
* Main links: Advert-Categories, Advert-Items, Graphs (static)
* Upload Files/Images - not implemented yet, but api available for models

--------------------- NODE server (JS/FrontEnd) -----------------------

* NOTE: In this version, CRUD isn't available for AdvertItem, only for AdvertCat
* NOTE: Cors issue when used fetch() requests in Chrome:
    1) chrome://extensions/ - Allow-Control-Allow-Origin: *
    2) in appache http-vhosts.conf at <VirtualHost 127.0.0.01:80> add:
        * Header set Access-Control-Allow-Origin "*"
        * Header set Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"
        * Header set Access-Control-Allow-Headers "HTTP_X_REST_USERNAME, HTTP_X_REST_PASSWORD"
    3) using - {mode: 'cors'} in fetch()
