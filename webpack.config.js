var path = require('path');
var webpack = require('webpack');

const PORT = 3000;

module.exports = {
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        inline: true,
        contentBase: './src',
        port: PORT
    },
    entry: [
        './dev/js/index.js'
    ],
    module: {
        loaders: [
            {
                test: /\.(js|jsx)?$/,
                exclude: [/node_modules/],
                loader: 'babel'
            },
            {
                test: /\.scss/,
                loader: 'style-loader!css-loader!sass-loader'
            }
        ]
    },
    resolve: {
      extensions: ['', '.js', '.jsx', '.css'],
      modulesDirectories: [
        'node_modules'
      ]
    },
    output: {
        publicPath: '/',
        path: 'src',
        filename: 'js/bundle.min.js'
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin()
    ]
};
